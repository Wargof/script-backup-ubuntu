#!/bin/bash

#   _____              _         _         _         ____                _                  
#  / ____|            (_)       | |       | |       |  _ \              | |                 
# | (___    ___  _ __  _  _ __  | |_    __| |  ___  | |_) |  __ _   ___ | | __ _   _  _ __  
#  \___ \  / __|| '__|| || '_ \ | __|  / _` | / _ \ |  _ <  / _` | / __|| |/ /| | | || '_ \ 
#  ____) || (__ | |   | || |_) || |_  | (_| ||  __/ | |_) || (_| || (__ |   < | |_| || |_) |
# |_____/  \___||_|   |_|| .__/  \__|  \__,_| \___| |____/  \__,_| \___||_|\_\ \__,_|| .__/ 
#                        | |                                                         | |    
#                        |_|                                                         |_|    

echo "   _____              _         _         _         ____                _            		 "
echo "  / ____|            (_)       | |       | |       |  _ \              | |                 "
echo " | (___    ___  _ __  _  _ __  | |_    __| |  ___  | |_) |  __ _   ___ | | __ _   _  _ __  "
echo "  \___ \  / __|| '__|| || '_ \ | __|  / _\` | / _ \ |  _ <  / _\` | / __|| |/ /| | | || '_ \ "
echo "  ____) || (__ | |   | || |_) || |_  | (_| ||  __/ | |_) || (_| || (__ |   < | |_| || |_) |"
echo " |_____/  \___||_|   |_|| .__/  \__|  \__,_| \___| |____/  \__,_| \___||_|\_\ \__,_|| .__/ "
echo "                        | |                                                         | |    "
echo "                        |_|                                                         |_|    "

	### VARIABLES ###
backup_dir="/media/Backup/BackupScript/."
diskstock="/media/Stockage"
diskdata="/media/Data"
diskvm="/media/VM"
homedir="/home/wargof"

### Tableau ###

# Les tableau serviront à y ajouter les répertoires à sauvegarder. 
# Une boucle for éxécutera une fonction Rsync pour chaque élément des tableaux.

# Divers
arraydivers=( \
	"$homedir" \
	"$diskvm" \
)
# Stockage
arraystock=( \
	"$diskstock/3D" \
	"$diskstock/Certificats_formations" \
	"$diskstock/Crypto" \
	"$diskstock/Divers" \
	"$diskstock/Cours_Formations_Tutoriels" \
	"$diskstock/Crypto" \
	"$diskstock/Divers" \
	"$diskstock/Documents" \
	"$diskstock/eBook" \
	"$diskstock/Emploi" \
	"$diskstock/Films&Séries" \
	"$diskstock/Formation_BONS" \
	"$diskstock/Graphisme_Vidéo" \
	"$diskstock/Images" \
	"$diskstock/Jeux" \
	"$diskstock/KeePass" \
	"$diskstock/Modèles" \
	"$diskstock/Musique" \
	"$diskstock/Openclassroom" \
	"$diskstock/OS" \
	"$diskstock/Partage" \
	"$diskstock/PCs" \
	"$diskstock/PGP" \
	"$diskstock/Programmation" \
	"$diskstock/Programmes" \
	"$diskstock/Projets-Perso" \
	"$diskstock/RPG-Maker" \
	"$diskstock/Sites-Webs" \
	"$diskstock/Wamp" \
	"$diskstock/Web" \
	"$diskstock/Wine" \
	"$diskstock/Xbox" \
)
# Data
arraydata=( \
	"$diskdata/Crypto-monnaie" \
	"$diskdata/DockerData" \
	"$diskdata/Hacking" \
	"$diskdata/Launchers" \
	"$diskdata/Modèles" \
	"$diskdata/Messages-vocals" \
	"$diskdata/Partage" \
	"$diskdata/PGP" \
	"$diskdata/Veracrypt" \
)

	### FONCTIONS ###
# Commande rsync : rsync -vaEz --delete-during --progress $SOURCE $DESTINATION
function rsyncparam() {
	rsync -vaEzr --progress --delete-during $1 $2
	sleep 3
}

	### INSTRUCTIONS ###

### Stockage ###
for i in "${arraystock[@]}"		# $i va prendre la valeur du premier élément du tableau, puis celle du 2ème, etc ..
do
	rsyncparam $i $backup_dir	# On appel la fonction custom rsyncparam() avec $i ( dossier ) et $backup_dir ( destination )
	sleep 3
done 
### Divers ###
for i in "${arraydivers[@]}"		# $i va prendre la valeur du premier élément du tableau, puis celle du 2ème, etc ..
do
	rsyncparam $i $backup_dir	# On appel la fonction custom rsyncparam() avec $i ( dossier ) et $backup_dir ( destination )
	sleep 3
done 
### VM ###
rsyncparam $diskvm $backup_dir
sleep 3
### Data ###
for i in "${arraydata[@]}"		# $i va prendre la valeur du premier élément du tableau, puis celle du 2ème, etc ..
do
	echo $i;					# On affiche la valeur de $i ( debug )
	rsyncparam $i $backup_dir	# On appel la fonction custom rsyncparam() avec $i ( dossier ) et $backup_dir ( destination )
	sleep 3
done 
